<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.lang.Math" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.nio.file.Files" %>
<%@ page import="java.nio.file.Paths" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%!
  Double getDistance(Double a, Double b, Double c, Double x, Double y)
  {
    return Math.abs(a * x + b * y + c) / Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
  }
%>
<%
  String jspPath          = getServletContext().getRealPath("/");
  String fileName         = jspPath + java.io.File.separator + "points.txt";
  String aParam           = request.getParameter("a");
  String bParam           = request.getParameter("b");
  String cParam           = request.getParameter("c");
  String maxDistanceParam = request.getParameter("max-distance");
  Double a                = null;
  Double b                = null;
  Double c                = null;
  Double maxDistance      = null;
  Boolean hasParams       = (aParam == null || bParam == null || cParam == null || maxDistanceParam == null) ? false : true;
  Boolean isValidParams   = true;

  try
  {
    a           = Double.parseDouble(aParam);
    b           = Double.parseDouble(bParam);
    c           = Double.parseDouble(cParam);
    maxDistance = Double.parseDouble(maxDistanceParam);
    // A и B одновременно не могут быть равны 0
    if (a == 0 && b == 0) isValidParams = false;
  }
  catch (Exception e)
  {
    isValidParams = false;
  }

  List<List<Double>> points = new ArrayList<List<Double>>();

  try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName)))
  {
    points = br
      .lines()
      .map(
        line -> new ArrayList<String>(Arrays.asList(line.split(",")))
          .stream()
          .map(Double::parseDouble)
          .collect(Collectors.toList())
      )
      .collect(Collectors.toList());
  }
  catch (IOException e)
  {
    e.printStackTrace();
  }

  List<List<Double>> searchResults = new ArrayList<List<Double>>();

  if (hasParams)
  {
    for (List<Double> point : points)
    {
      Double x = point.get(0);
      Double y = point.get(1);
      if (getDistance(a, b, c, x, y) < maxDistance)
      {
        searchResults.add(point);
      }
    }
  }
%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">4. Взаимодействие HTML/JSP-сервлет</div>
  <div class="desc">
    10) Информация о точках на плоскости хранится в файле. <br>
    Выбрать все точки, наиболее приближенные к заданной прямой. <br>
    Параметры прямой и максимальное расстояние от точки до прямой вводятся на стороне клиента.
  </div>

  <br>

  <form action="" method="get">
    <div class="input-group">
      <label>Введите параметры прямой:</label>
      <input name="a" type="text" placeholder="A" class="input-small" value="<%= hasParams ? aParam : "" %>">
      x +
      <input name="b" type="text" placeholder="B" class="input-small" value="<%= hasParams ? bParam : "" %>">
      y +
      <input name="c" type="text" placeholder="C" class="input-small" value="<%= hasParams ? cParam : "" %>">
      = 0
    </div>
    <div class="input-group">
      <label for="max-distance">Введите максимальное расстояние от точки до прямой:</label>
      <input name="max-distance" type="text" id="max-distance" value="<%= hasParams ? maxDistanceParam : "" %>">
    </div>
    <button>Найти</button>
  </form>

  <% if (hasParams) { %>
    <% if (isValidParams) { %>
      <div>
        <span class="title">Ближайшие точки</span>
        <div>
          <% if (searchResults != null) { %>
            <% for (int i = 0; i < searchResults.size(); ++i) { %>
              <pre>[<%= searchResults.get(i).get(0) %>, <%= searchResults.get(i).get(1) %>]</pre>
            <% } %>
          <% } else { %>
            <span>Ничего не найдено</span>
          <% } %>
        </div>
      </div>
    <% } else { %>
      Пожалуйста, введите правильные параметры
    <% } %>
  <% } %>
</body>
</html>